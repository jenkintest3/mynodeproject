const express = require('express')

const routeProduct = require('./Router/product')

const app =express();

app.use('/product',routeProduct)

app.listen(9898,'0.0.0.0',()=>{
    console.log('server started at port no 9898');
});
