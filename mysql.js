const mydb =require('mysql2')

const pool = mydb.createPool({
    host:'localhost',
    user:'root',
    password:'manager',
    waitForConnections: true,
    connectionLimit:10,
    database:'advjava2'
})

module.exports={
    pool
}